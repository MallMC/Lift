'use strict';
const request = require('request');
const logger = require('./logger');
const baseURL = 'https://api.creeper.host/';
class Aries {

    constructor(key, secret){
        this.key = key;
        this.secret = secret;
    }

    spinupMinigame(duration, customURL, ram, callback){
        var data ={
          ram: ram,
          game: "custom",
          time: duration.toString(),
          custom: customURL,
        };
        logger.info(JSON.stringify(data));
        this.makeAPIRequest('POST', 'billing/spinupminigame', data, (body) => {
            if(body.status === "error"){
                return callback(JSON.stringify(body));
            }
            var server = {
                ip: body.ip,
                port: body.port,
                uuid: body.uuid,
            };
            return callback(undefined, server);
        })
    }

    getMinigames(callback){
        this.makeAPIRequest('POST', 'billing/listMinigames', {}, (body) => {
            if(body.status === "error"){
                return callback(JSON.stringify(body));
            }
            return callback(undefined, body);
        });
    }

    getTimeLeft(uuid, callback){
        this.makeAPIRequest('POST', 'billing/timerMinigame', { uuid: uuid}, (body) =>{
            if(body.status === "error"){
                return callback(JSON.stringify(body));
            }
            return callback(undefined, body);
        });
    }

    spinDownMinigame(uuid, callback){
        this.makeAPIRequest('POST', 'billing/spindownminigame', {
            uuid: uuid
        }, (body) => {
            if(body.status === "error"){
                return callback(JSON.stringify(body));
            }
            return callback(undefined, body.message);
        });
    }

    getCredit(callback){
        this.makeAPIRequest('GET', 'billing/credit', {}, (body) => {
            if(body.status === "error"){
                return callback(JSON.stringify(body));
            }
            return callback(undefined, body.credit);
        });
    }

    makeAPIRequest(method, endpoint, data, callback){
        data['key'] = this.key;
        data['secret'] = this.secret;
        var options = {
          url: baseURL + endpoint,
          headers: {
            'key': this.key,
            'secret': this.secret
          },
          method: method,
          json:true,
          body: data,
        };
        request(options, (error, response, body) => {
            if(error){
                logger.info(error);
                return;
            }
            if(response.statusCode != 200 && resposne.statusCode != 300){
                logger.info('Request failed. URL: %s , Code: %s , response: %s', baseURL + endpoint, response.statusCode, body);
                return;
            }
            callback(body);
        });
    }

}

module.exports = Aries;