const Slack = require('slack-node');
const logger = require('./logger');

class SlackClient {

    constructor(webhookURL){
        this.webhook = webhookURL;
        this.slack = new Slack();
        this.slack.setWebhook(this.webhook);
    }

    sendMessage(channel, username,icon,  message){
        this.slack.webhook({
        channel: "#"+channel,
        username: username,
        icon_emoji: icon,
        text: message
    }, function(err, response) {
        if(err){
            logger.info(err);
            return;
        }
        if(response.statusCode === 200){
            logger.info("Sent slack message");
        }
        });
    }
}

module.exports = SlackClient;