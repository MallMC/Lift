const logger = require('./logger');
const Aries = require('./aries');
const Slack = require('./slack');
const Redis = require('./redis');
require('dotenv').config();

const gameBaseURL = "http://cdn.redstone.tech/Rushmeadfiles/mallmc/gamemodes/";

logger.info('Starting up the lift');
var aries = new Aries(process.env.API_KEY, process.env.API_SECRET);
var slack = new Slack(process.env.WEBHOOK_URL);
var redis = new Redis(process.env.REDIS_PORT, process.env.REDIS_IP, process.env.REDIS_PASSWORD, process.env.REDIS_DB);
logger.info('Connected to aries');
aries.getCredit((err, credit) => {
    if (err) {
        logger.info(new Error(err));
        return;
    }
    logger.info('Current credit: £%s', credit);
    if (credit < process.env.CREDIT_ALERT) {
        logger.info('Credit is lower than £10. Sending slack message');
        slack.sendMessage("status", "MallLift", ":ghost:", "CreeperHost credit is lower than £" + process.env.CREDIT_ALERT + ", please add some more :smile:");
    }
});
aries.getMinigames((err, games) => {
    if (err) {
        logger.info(new Error(err));
        return;
    }
    logger.info('Current servers: %s', JSON.stringify(games));
});
logger.info('Subscribing to newServer redis');
redis.subscribe("newServer", (message) => {
    logger.info("Network is requesting we spin up a server, gathering details");
    var details = message.split(":");
    var serverGame = details[0];
    var serverGameVersion = details[1];
    logger.info("Network has requested new spin up of a %s @ %s", serverGame, serverGameVersion);
    var gameURL = gameBaseURL + serverGame + "/" + serverGame + "-" + serverGameVersion + ".zip";
    logger.info("Game URL should be %s", gameURL);
    aries.spinupMinigame(1, gameURL, 1, (err, server) => {
        if (err) {
            logger.info(new Error(err));
            return;
        }
        logger.info("Spun up server: %s", JSON.stringify(server));
        redis.publish("addServer", server.uuid + ":" + server.ip + ":" + server.port +":"+serverGame);
    });
});
redis.subscribe("gameEnd", (message) => {
    logger.info("A game has ended, lets decide if we need that server or not");
    var details = message.split(":");
    var serverUUID = details[0];
    var serverGame = details[1];
    var queueSize = details[2];
    aries.getTimeLeft(serverUUID, (message) => {
        var mins = message.remaining / 60;
        logger.info("Server has %s minutes left", mins);
        if (mins < 5) {
            aries.spinDownMinigame(serverUUID, (message) => {
                logger.info("Spun down server: %s", message);
                redis.publish("removeServer", serverUUID);
            });
            return;
        }
    });
    if (queueSize <= 2) {
        aries.spinDownMinigame(serverUUID, (message) => {
            logger.info("Spun down server: %s", message);
            redis.publish("removeServer", serverUUID);
        });
    }
});
