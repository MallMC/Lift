const bunyan = require('bunyan');
const logger = bunyan.createLogger({name: "MallLift"});

module.exports = logger;