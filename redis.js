const Redis = require('ioredis');

class RedisClient {

    constructor(port, ip, password, db){
        this.subs = {};
        this.redisSub = new Redis({
            port: port,
            host: ip,
            family: 4,
            password: password,
            db: db
        });
        this.redisPub = new Redis({
            port: port,
            host: ip,
            family: 4,
            password: password,
            db: db
        });
        this.redisSub.on('message', (channel, message) => {
            this.subs[channel].forEach((callback) => {
                callback(message);
            });
        });
    }

    subscribe(channel, callback){
        this.redisSub.subscribe(channel);
        if(this.subs[channel] === undefined){
            this.subs[channel] = [callback];
        }else{
            this.subs[channel].append(callback);
        }
    }

    publish(channel, message){
        this.redisPub.publish(channel, message);
    }

}

module.exports = RedisClient;